# Java Task 02

## Описание

1. Познакомиться с Git
2. Создать классы Credit, Transfer, Client, переопределить equals, hashcode,
   toString (чтобы объект печатался в формате JSON или XML)
3. Создать интерфейс CreditService и его реализацию, с методами получения
   кредита и погашения кредита по клиенту
4. Сдать ДЗ через pull request

## Запуск

Для сборки программы необходимо выполнить:

```bash
mvn clean compile package
```

Запуск JAR-файла:

```bash
java -jar ./target/build-1.0-jar-with-dependencies.jar
```
