package fun.zsxoff;


import fun.zsxoff.bank.Client;
import fun.zsxoff.bank.CreditServiceImpl;
import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import fun.zsxoff.bank.Exceptions.NotEnoughMoneyException;
import fun.zsxoff.bank.Transfer;

public class Main {
    public static void main(String[] args) throws NegativeMoneyException, NotEnoughMoneyException {

        // Create clients.
        Client client1 = new Client("John Doe", 100);
        Client client2 = new Client("Jane Roe", 50);

        // Transfer money from Client1 to Client2.
        Transfer.transferMoney(client1, client2, 25);

        // Create CreditService.
        CreditServiceImpl creditService = new CreditServiceImpl();

        // Get credits to clients.
        creditService.giveCredit(client1, creditService.createCredit(500));
        creditService.giveCredit(client1, creditService.createCredit(600));
        creditService.giveCredit(client1, creditService.createCredit(700));

        creditService.giveCredit(client2, creditService.createCredit(1000));
        creditService.giveCredit(client2, creditService.createCredit(1500));

        System.out.println(client1);
        System.out.println(client2);
    }
}
