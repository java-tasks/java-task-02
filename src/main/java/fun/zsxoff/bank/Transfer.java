package fun.zsxoff.bank;


import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import fun.zsxoff.bank.Exceptions.NotEnoughMoneyException;

public class Transfer {
    public static void transferMoney(Client clientFrom, Client clientTo, double summa)
            throws NegativeMoneyException, NotEnoughMoneyException {
        if (summa <= 0) {
            throw new NegativeMoneyException();
        }

        if (clientFrom.getCash() - summa < 0) {
            throw new NotEnoughMoneyException();
        }

        clientFrom.setCash(clientFrom.getCash() - summa);
        clientTo.setCash(clientTo.getCash() + summa);
    }
}
