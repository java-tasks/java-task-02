package fun.zsxoff.bank;


import com.google.gson.GsonBuilder;
import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import java.util.Objects;
import java.util.UUID;

public class Credit {
    private final UUID uid;
    private double summa;

    public Credit() {
        this.uid = UUID.randomUUID();
        this.summa = 0;
    }

    public Credit(double summa) throws NegativeMoneyException {
        this();
        this.setSumma(summa);
    }

    public UUID getUid() {
        return uid;
    }

    public double getSumma() {
        return summa;
    }

    protected void setSumma(double summa) throws NegativeMoneyException {
        if (summa < 0) {
            throw new NegativeMoneyException();
        }

        this.summa = summa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return Double.compare(credit.summa, summa) == 0 && uid.equals(credit.uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, summa);
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
