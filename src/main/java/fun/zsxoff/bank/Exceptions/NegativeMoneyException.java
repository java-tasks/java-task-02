package fun.zsxoff.bank.Exceptions;

public class NegativeMoneyException extends Exception {
    public NegativeMoneyException() {}

    public NegativeMoneyException(String message) {
        super(message);
    }

    public NegativeMoneyException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeMoneyException(Throwable cause) {
        super(cause);
    }

    public NegativeMoneyException(
            String message,
            Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
