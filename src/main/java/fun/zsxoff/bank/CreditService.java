package fun.zsxoff.bank;


import fun.zsxoff.bank.Exceptions.NegativeMoneyException;
import java.util.UUID;

public interface CreditService {
    public Credit createCredit(double summa) throws NegativeMoneyException;

    public void giveCredit(Client client, Credit credit);

    public void removeCredit(Client client, Credit credit);

    public void removeCredit(Client client, UUID creditUUID);
}
